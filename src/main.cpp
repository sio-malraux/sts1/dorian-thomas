#include <libpq-fe.h>
#include <iostream>
#include <string.h>
#include <iomanip>
//https://docs.postgresql.fr/9.6/libpq-exec.html (doc)

using namespace std;//déclaration de l'utilisation des fonctions avec l'espace-nom std::

//déclaration des fonctions (sous-programmes)
void affichage_info(PGconn *connexion);
void mdp(PGconn *connexion);
int calcul_taille_champs(PGresult *exec);
int calcul_max_donnees(int separation_nomC, PGresult *exec);
void modif_memoire_descriptions(PGresult *exec, int separation_nomC);
void separation_h(PGresult *exec, int separation_nomC);
void affichage_nom_colonne(PGresult *exec, int separation_nomC);
void affichage_donnees(PGresult *exec, int separation_nomC);
void gestion_erreur(ExecStatusType resultat);

int main()
{
  int code_retour = 0; //retour du programme (erreur ou non, 0 ou 1)
  char cominfo[] = "host=postgresql.bts-malraux72.net port=5432 user=t.raymond password=P@ssword";//informations de connexion au serveur bdd
  PGPing ping = PQping(cominfo); //ping au serveur bdd

  if(ping == PQPING_OK)
  {
    cout << "Serveur accessible" << endl;
    PGconn *connexion = PQconnectdb(cominfo); //connexion à la bdd

    if(PQstatus(connexion) == CONNECTION_OK) //la connexion est établie
    {
      affichage_info(connexion);//fonction affichage information bdd
      const char requete[] = "SET schema 'si6';  SELECT \"Animal\".id,\"Animal\".nom AS \"nom de l'animal\" ,sexe,date_naissance AS \"date de naissance\" ,commentaires,\"Race\".nom as race, description FROM \"Animal\" INNER JOIN \"Race\" ON \"Animal\".race_id = \"Race\".id WHERE sexe = 'Femelle' AND \"Race\".nom = 'Singapura';"; //chaîne de caractère contenant la requête
      PGresult *exec = PQexec(connexion, requete); //variable stockant les données de l'éxecution de la requête
      ExecStatusType resultat = PQresultStatus(exec); //variable stockant le résultat de la requête

      if(resultat == PGRES_TUPLES_OK) //résultat requête ok
      {
        int separation_nomC = calcul_taille_champs(exec); //appel de la fonction calcul_taille_champs retournant un entier
        int separation_champs = calcul_max_donnees(separation_nomC, exec); //appel de la fonction calcul_max_donnees retournant un entier
        modif_memoire_descriptions(exec, separation_nomC); //appel de la fonction modif_memoire_descriptions
        separation_h(exec, separation_nomC); //appel de la fonction separation_h
        affichage_nom_colonne(exec, separation_nomC); //appel de la fonction affichage_nom_colonne
        separation_h(exec, separation_nomC);
        affichage_donnees(exec, separation_nomC); ////appel de la fonction affichage_donnees
        separation_h(exec, separation_nomC);
        cout << "L'exécution de la requête SQL a retourné " << PQntuples(exec) << " enregistrements." << endl;
        PQclear(exec); //vide les données recupérées de la requête
      }
      else
      {
        gestion_erreur(resultat);////appel de la fonction gestion_erreur
      }
    }
    else
    {
      cerr << "Connexion non établie" << endl;
      code_retour = 1;
    }
  }
  else
  {
    cerr << "Serveur non accessible" << endl;
    code_retour = 1;
  }

  return code_retour;
}
//fonction affichage information bdd
void affichage_info(PGconn *connexion)
{
  cout << "La connexion au serveur de base de donnée '" << PQhost(connexion) << "' a été établie avec les paramètres suivants :" << endl;
  cout << "utilisateur : " << PQuser(connexion) << endl;
  cout << "mot de passe : ";
  mdp(connexion); //appel de la fonction mdp
  cout << endl;
  cout << "base de données : " << PQdb(connexion) << endl;
  cout << "port TCP : " << PQport(connexion) << endl;
  cout << "chiffrement SSL : ";
  int ssl = PQsslInUse(connexion);

  if(ssl == 1)
  {
    cout << "true" << endl;
  }
  else
  {
    cout << "false" << endl;
  }

  cout << "encodage : " << PQparameterStatus(connexion, "server_encoding") << endl;
  cout << "version du protocole : " << PQprotocolVersion(connexion) << endl;
  cout << "version du serveur : " << PQserverVersion(connexion) << endl;
  cout << "version de la bibliothèque ' libpq ' du client : " << PQlibVersion() << endl;
}
//fonction remplaçant la chaîne de caractères du mot de passe en "*"
void mdp(PGconn *connexion)
{
  char *motdepasse = PQpass(connexion);

  for(int i = 0; i < strlen(motdepasse); i++)
  {
    cout << "*";
  }
}
//fonction calculant la taille maxi des nom des colonnes du tableau (requête)
int calcul_taille_champs(PGresult *exec)
{
  int separation_nomC = 0;

  //calcul taille max des noms de colonnes
  for(int k = 0; k < PQnfields(exec); k++)
  {
    if(strlen(PQfname(exec, k)) > separation_nomC)
    {
      separation_nomC = strlen(PQfname(exec, k));
    }
  }

  return separation_nomC;
}
//fonction calculant la taille maxi des nom des données du tableau
int calcul_max_donnees(int separation_nomC, PGresult *exec)
{
  //calcule taille max des données
  int separation_champs = 0;

  for(int j = 0; j < PQntuples(exec); j++)
  {
    for(int k = 0; k < PQnfields(exec); k++)
    {
      if(PQgetlength(exec, j, k) > separation_nomC)
      {
        separation_champs = PQgetlength(exec, j, k);
      }
    }
  }

  return separation_champs;
}
//fonction modifiant la mémoire des chaines de caractères de description 1 et 2 pour les raccourcir avec trois points
void modif_memoire_descriptions(PGresult *exec, int separation_nomC)
{
  // les trois points pour description (modification de la mémoire)
  char *description1 = PQgetvalue(exec, 0, 6);
  description1[separation_nomC] = '\0';
  description1[separation_nomC - 1] = '.';
  description1[separation_nomC - 2] = '.';
  description1[separation_nomC - 3] = '.';
  char *description2 = PQgetvalue(exec, 1, 6);
  description2[separation_nomC] = '\0';
  description2[separation_nomC - 1] = '.';
  description2[separation_nomC - 2] = '.';
  description2[separation_nomC - 3] = '.';
}
//fonction affichage de la séparation horizontale (ligne permetant de visualiser un tableau)
void separation_h(PGresult *exec, int separation_nomC)
{
  int separation_horizontale = ((separation_nomC + 1) * PQnfields(exec) + 1); //+1 pour chaque barres de séparations et +1 pour la dernnière barres

  for(int i = 0; i < separation_horizontale; i++)
  {
    cout << "-";
  }

  cout << endl;
}
//fonction affichage des noms des colonnes du tableau
void affichage_nom_colonne(PGresult *exec, int separation_nomC)
{
  for(int i = 0; i < PQnfields(exec); i++)
  {
    cout << "|" << setw(separation_nomC) << left << PQfname(exec, i);
  }

  cout << "|" << endl;
}

//fonction affichage des données du tableau
void affichage_donnees(PGresult *exec, int separation_nomC)
{
  for(int j = 0; j < PQntuples(exec); j++)
  {
    for(int k = 0; k < PQnfields(exec); k++)
    {
      cout << "|" << setw(separation_nomC) << left << PQgetvalue(exec, j, k);
    }

    cout << "|" << endl;
  }
}
//fonction gêrant les erreurs de résultats de la requête
void gestion_erreur(ExecStatusType resultat)
{
  if(resultat == PGRES_EMPTY_QUERY)
  {
    cerr << "La chaîne envoyée au serveur était vide." << endl;
  }
  else if(resultat == PGRES_COMMAND_OK)
  {
    cerr << "Fin avec succès d'une commande ne renvoyant aucune donnée." << endl;
  }
  else if(resultat == PGRES_COPY_OUT)
  {
    cerr << "Début de l'envoi (à partir du serveur) d'un flux de données." << endl;
  }
  else if(resultat == PGRES_COPY_IN)
  {
    cerr << "Début de la réception (sur le serveur) d'un flux de données." << endl;
  }
  else if(resultat == PGRES_BAD_RESPONSE)
  {
    cerr << "La réponse du serveur n'a pas été comprise." << endl;
  }
  else if(resultat == PGRES_NONFATAL_ERROR)
  {
    cerr << "Une erreur non fatale (une note ou un avertissement) est survenue." << endl;
  }
  else if(resultat == PGRES_FATAL_ERROR)
  {
    cerr << "Une erreur fatale est survenue." << endl;
  }
  else if(resultat == PGRES_COPY_BOTH)
  {
    cerr << "Lancement du transfert de données Copy In/Out (vers et à partir du serveur). Cette fonctionnalité est seulement utilisée par la réplication en flux, so this status should not occur in ordinary applications." << endl;
  }
  else if(resultat == PGRES_SINGLE_TUPLE)
  {
    cerr << "La structure PGresult contient une seule ligne de résultat provenant de la commande courante. Ce statut n'intervient que lorsque le mode simple ligne a été sélectionné pour cette requête." << endl;
  }
}
